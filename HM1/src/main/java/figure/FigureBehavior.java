package figure;

public interface FigureBehavior {

//  show figure
    Point show();

//  move figure
    void move(double moveX, double moveY);

//  scaling figure
    void scale(double koeff) throws IllegalArgumentException;

//  counting area of figure
    double area();

}
