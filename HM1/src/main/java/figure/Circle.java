package figure;

/**
 * Класс круга
 *
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 * @see AbstractFigure
 */
public class Circle extends AbstractFigure {

    private double radius;

    public Circle(){}

    public Circle(double radius) throws IllegalArgumentException{
        setRadius(radius);
    }

    public Circle(double radius,
                  double x, double y) throws IllegalArgumentException{
        super(x,y);
        if(radius <= 0) throw new IllegalArgumentException("radius can be positive");
        this.radius = radius;
    }

    /**
     * Метод масштабирования круга
     * @param koeff коэффициент масштабирования -
     *              число типа {@code double}
     * @throws IllegalArgumentException
     */
    @Override
    public void scale(double koeff) throws IllegalArgumentException{
        if (koeff <= 0) throw new IllegalArgumentException("koeff can be positive");
        radius *= koeff;
    }

    /**
     * Метод подсчёта площади круга
     * @return площадь круга - число типа {@code double}
     */
    @Override
    public double area() {
        return Math.PI*Math.pow(radius, 2);
    }

    /**
     * Проверка на нахождение точки в круге
     * @param point - параметр, содержащий координаты точки
     * @return {@code true} - точка находится в круге,
     *          {@code false} - точка находится вне круга
     */
    public boolean checkPoint(Point point){
        double d = Math.sqrt(Math.pow(point.getX() - this.coordinates.getX(), 2) +
                Math.pow(point.getY() - this.coordinates.getY(), 2));
        return d <= radius;
    }

    /**
     * Проверка на вхождение одного круга в другой
     * @param circle - объект круга
     * @return {@code true} - в круге находится другой круг
     *          {@code false} - иначе
     */
    public boolean ckeck(Circle circle){
        double d = Math.sqrt(Math.pow(circle.coordinates.getX() - this.coordinates.getX(), 2) +
                Math.pow(circle.coordinates.getY() - this.coordinates.getY(), 2));
        return  d + radius <= circle.radius || d + circle.radius <= radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) throws IllegalArgumentException {
        if (radius <= 0) throw new IllegalArgumentException("radius can be positive");
        this.radius = radius;
    }
}
