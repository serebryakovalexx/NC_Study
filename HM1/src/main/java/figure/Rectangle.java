package figure;

/**
 * Сущность, описывающая прямоугольник
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public class Rectangle extends AbstractFigure {

    private double length;
    private double width;

    public Rectangle() {}

    public Rectangle(double length, double width) throws IllegalArgumentException {
        setLength(length);
        setWidth(width);
    }

    public Rectangle(double length, double width,
                     double x, double y) throws IllegalArgumentException {
        super(x, y);
        setLength(length);
        setWidth(width);
    }

    /**
     * Метод масштабирования прямоугольника
     *
     * @param koeff коэффициент масштабирования -
     *              число типа {@code double}
     * @throws IllegalArgumentException
     */
    @Override
    public void scale(double koeff) {
        if (koeff <= 0) throw new IllegalArgumentException();
        length *= koeff;
        width *= koeff;
    }

    /**
     * Метод подсчёта площади прямоугольника
     *
     * @return площадь круга - число типа {@code double}
     */
    @Override
    public double area() {
        return width * length;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) throws IllegalArgumentException {
        if (length <= 0) throw new IllegalArgumentException("length must be positive");
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) throws IllegalArgumentException {
        if (width <= 0) throw new IllegalArgumentException("width must be positive");
        this.width = width;
    }
}
