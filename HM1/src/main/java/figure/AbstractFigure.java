package figure;

/**
 * Абстрактный класс фигуры
 *
 * @author Alex Serrebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public abstract class AbstractFigure implements FigureBehavior{

    protected Point coordinates;

    protected AbstractFigure(){
        coordinates = new Point();
    }

    protected AbstractFigure(double x, double y){
        coordinates = new Point(x,y);
    }

    /**
     * Метод перемещения фигуры
     * @param moveX сдвиг по оси X
     * @param moveY сдвиг по оси Y
     */
    @Override
    public void move(double moveX, double moveY){
        setCoordinates(new Point(this.coordinates.getX() + moveX,
                this.coordinates.getY() + moveY));
    }

    /**
     * Вывод координат центра фигуры
     * @return координаты фигуры типа {@link Point}
     * @see Point
     */
    @Override
    public Point show(){
        return coordinates;
    }

    protected Point getCoordinates() {
        return coordinates;
    }

    protected void setCoordinates(Point coordinates) {
        this.coordinates = coordinates;
    }
}
