package figure;

/**
 * Сущность, описывающая треугольник
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public class Triangle extends AbstractFigure {

    private double firstSide;
    private double secondSide;
    private double thirdSide;

    public Triangle(){}

    public Triangle(double firstSide, double secondSide, double thirdSide) throws IllegalArgumentException{
        if (firstSide <= 0 || secondSide <=0 || thirdSide <=0 ||
                firstSide >= thirdSide + secondSide ||
                secondSide >= firstSide + thirdSide ||
                thirdSide >= firstSide + secondSide) throw new IllegalArgumentException();
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;
    }

    public Triangle(double firstSide, double secondSide, double thirdSide,
                    double x, double y) throws IllegalArgumentException{
        super(x,y);
        if (firstSide <= 0 || secondSide <=0 || thirdSide <=0 ||
                firstSide >= thirdSide + secondSide ||
                secondSide >= firstSide + thirdSide ||
                thirdSide >= firstSide + secondSide) throw new IllegalArgumentException();
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;
    }

    /**
     * Метод масштабирования треугоьник
     * @param koeff коэффициент масштабирования -
     *              число типа {@code double}
     * @throws IllegalArgumentException
     */
    @Override
    public void scale(double koeff) {
        if (koeff <= 0) throw new IllegalArgumentException();
        firstSide *= koeff;
        secondSide *= koeff;
        thirdSide *= koeff;
    }

    /**
     * Метод подсчёта площади треугольника
     * @return площадь треугольника - число типа {@code double}
     */
    @Override
    public double area() {
        double p = (firstSide + secondSide + thirdSide)/2;
        return Math.sqrt(p*(p-firstSide)*(p-secondSide)*(p-thirdSide));
    }

    public double getFirstSide() {
        return firstSide;
    }

    public void setFirstSide(double firstSide) throws IllegalArgumentException{
        if (firstSide <= 0 ||
                firstSide >= thirdSide + secondSide ||
                secondSide >= firstSide + thirdSide ||
                thirdSide >= firstSide + secondSide) throw new IllegalArgumentException();
        this.firstSide = firstSide;
    }

    public double getSecondSide() {
        return secondSide;
    }

    public void setSecondSide(double secondSide) throws IllegalArgumentException{
        if (secondSide <= 0 ||
                firstSide >= thirdSide + secondSide ||
                secondSide >= firstSide + thirdSide ||
                thirdSide >= firstSide + secondSide) throw new IllegalArgumentException();
        this.secondSide = secondSide;
    }

    public double getThirdSide() {
        return thirdSide;
    }

    public void setThirdSide(double thirdSide) throws IllegalArgumentException{
        if (thirdSide <= 0 ||
                firstSide >= thirdSide + secondSide ||
                secondSide >= firstSide + thirdSide ||
                thirdSide >= firstSide + secondSide) throw new IllegalArgumentException();
        this.thirdSide = thirdSide;
    }

}
