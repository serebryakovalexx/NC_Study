package figure;

/**
 * Сущность, описывающая координаты точки, фигуры
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public class Point {

    private double x;
    private double y;

    public Point(){}

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

}
