package employee;

import java.util.Comparator;

/**
 * Компаратор реализует сравнение работников
 * по полю <b>lastName</b>
 *
 * @author Alex Serrebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public class EmployeeLastNameComparator  implements Comparator<Employee>{

    /**
     * реализация метода compare() интерфейса {@link Comparator}
     * @param e1 объект типа {@link Employee},
     * @param e2 объект типа {@link Employee}
     * @return значение типа {@code int}, полученное
     *         при применение метода compare() класса {@link String}
     */
    @Override
    public int compare(Employee e1, Employee e2){
        return e1.getLastName().compareTo(e2.getLastName());
    }
}
