package employee;

/**
 * Сущность работника
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */

public final class Employee{

    private static final Position DEFAULT_POSITION = Position.CLERK;
    private static final double DEFAULT_SALARY = 10_000;

    private String firstName;
    private String lastName;
    private Position position;
    private double salary;

    public Employee(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = DEFAULT_POSITION;
        this.salary = DEFAULT_SALARY;
    }

    public Employee(String firstName, String lastName,
                    Position position){
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.salary = DEFAULT_SALARY;
    }

    public Employee(String firstName, String lastName,
                    String position){
        this(firstName, lastName, Position.posByName(position));
    }

    public Employee(String firstName, String lastName,
                    double salary){
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = DEFAULT_POSITION;
        this.salary = salary;
    }

    public Employee(String firstName, String lastName,
                    Position position, double salary){
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.salary = salary;
    }

    public Employee(String firstName, String lastName,
                    String position, double salary){
        this(firstName, lastName, Position.posByName(position), salary);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString(){
        return "Employee:{" +
                "\nfirstName : " + firstName +
                ",\nlastName : " + lastName +
                ",\nposition : " + position.stringName() +
                ",\nsalary : " + salary +
                "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (Double.compare(employee.getSalary(), getSalary()) != 0) return false;
        if (getFirstName() != null ? !getFirstName().equals(employee.getFirstName()) : employee.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(employee.getLastName()) : employee.getLastName() != null)
            return false;
        return getPosition() == employee.getPosition();
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPosition() != null ? getPosition().hashCode() : 0);
        temp = Double.doubleToLongBits(getSalary());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
