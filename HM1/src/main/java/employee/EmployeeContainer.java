package employee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Контейнер работников ({@link Employee}).
 * Содержит методы для работы с контейнером
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public class EmployeeContainer {

    private List<Employee> employees = new ArrayList<>();

    /**
     * Добавление работника через консоль
     * (реализуется через {@link Scanner})
     * @throws IllegalArgumentException
     */
    public void CLIAddEmployee() throws IllegalArgumentException{

        String firstName, lastName, position, salary;

        Scanner scanner = new Scanner(System.in);
        if ((firstName = scanner.next()).isEmpty()){
            throw new IllegalArgumentException();
        }
        if ((lastName = scanner.next()).isEmpty()){
            throw new IllegalArgumentException();
        }
        position = scanner.next();
        salary = scanner.next();

        if (position.equals("0") && salary.equals("0")) {
            employees.add(new Employee(firstName, lastName));
        } else if (!position.equals("0") && salary.equals("0")){
            employees.add(new Employee(firstName, lastName, position));
        } else if (position.equals("0") && !salary.equals("0")){
            employees.add(new Employee(firstName, lastName, Double.parseDouble(salary)));
        } else {
            employees.add(new Employee(firstName, lastName, position, Double.parseDouble(salary)));
        }
        scanner.close();
    }

    /**
     * Добавление работника через консоль
     * (реализуется через {@link BufferedReader})
     * @throws IllegalArgumentException
     */
//    public void CLIAddEmployee() throws IllegalArgumentException, IOException {
//
//        String firstName, lastName, position, salary;
//
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//
//        if ((firstName = reader.readLine()).isEmpty()){
//            throw new IllegalArgumentException();
//        }
//        if ((lastName = reader.readLine()).isEmpty()){
//            throw new IllegalArgumentException();
//        }
//
//        position = reader.readLine();
//        salary = reader.readLine();
//
//        if (position.equals("0") && salary.equals("0")) {
//            employees.add(new Employee(firstName, lastName));
//        } else if (!position.equals("0") && salary.equals("0")){
//            employees.add(new Employee(firstName, lastName, position));
//        } else if (position.equals("0") && !salary.equals("0")){
//            employees.add(new Employee(firstName, lastName, Double.parseDouble(salary)));
//        } else {
//            employees.add(new Employee(firstName, lastName, position, Double.parseDouble(salary)));
//        }
//
//        reader.close();
//    }

    /**
     * Вывод элементов контейнера в консоль
     */
//    public void showEmployees(){
//        for(Employee employee : employees){
//            System.out.println(employee);
//        }
//    }

    /**
     * Вывод элементов контейнера в консоль
     * (Реализовано через method reference)
     */
    public void showEmployees(){
        employees.forEach(System.out::println);
    }

    /**
     * Поиск работников в контейнере по заданной должности
     * @param position должность({@link Position}) работника
     * @return список работников типа {@link List},
     *         удовлетворяющих условию поиска,
     *         если таковых нет, то возвращается пустой список
     */
//    public List<Employee> search(Position position){
//        List<Employee> employeeList = new ArrayList<>();
//        for(Employee employee : employees){
//            if (employee.getPosition().equals(position)) {
//                employeeList.add(employee);
//            }
//        }
//        return employeeList;
//    }

    /**
     * Поиск работников в контейнере по заданной должности
     * (Реализовано через Stream API)
     * @param position должность({@link Position}) работника
     * @return список работников типа {@link List},
     *         удовлетворяющих условию поиска,
     *         если таковых нет, то возвращается пустой список
     */
    public List<Employee> search(Position position) {
        return employees
                .stream()
                .filter(employee -> employee.getPosition().equals(position))
                .collect(Collectors.toList());
    }

    /**
     * Поиск работников в контейнере по заданной должности
     * @param position именование должности - строка({@link String})
     * @return список работников типа {@link List},
     *         удовлетворяющих условию поиска,
     *         если таковых нет, то возвращается пустой список
     */
    public List<Employee> search(String position){
        return this.search(Position.posByName(position));
    }


    /**
     * Сортировка работников по полям <b>name</b> и <b>salary</b>
     * @return список ({@link List}) отсортированных работников
     */
//    public List<Employee> sortByLastNameAndSalary(){
//        List<Employee> list = new ArrayList<>(employees);
//        Comparator<Employee> comparator = new EmployeeLastNameComparator()
//                .thenComparing(new EmplloyeeSalaryComparator());
//        list.sort(comparator);
//        return list;
//    }

    /**
     * Сортировка работников по полям <b>lastName</b> и <b>salary</b>
     * (Реализовано через Stream API)
     * @return список ({@link List}) отсортированных работников
     */
    public List<Employee> sortByLastNameAndSalary(){
        Comparator<Employee> comparator = new EmployeeLastNameComparator()
                .thenComparing(new EmplloyeeSalaryComparator());
        return employees
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    /**
     * Сортировка работников по полю <b>lastName</b>
     * @return список ({@link List}) отсортированных работников
     */
//    public List<Employee> sortByLastName(){
//        List<Employee> list = new ArrayList<>(employees);
//        list.sort(new EmployeeLastNameComparator());
//        return list;
//    }

    /**
     * Сортировка работников по полю <b>lastName</b>
     * (Реализовано через Stream API)
     * @return список ({@link List}) отсортированных работников
     */
    public List<Employee> sortByLastName(){
        return employees
                .stream()
                .sorted(new EmployeeLastNameComparator())
                .collect(Collectors.toList());
    }

    /**
     * Сортировка работников по полю <b>salary</b>
     * @return список ({@link List}) отсортированных работников
     */
//    public List<Employee> sortBySalary() {
//        List<Employee> list = new ArrayList<>(employees);
//        list.sort(new EmplloyeeSalaryComparator());
//        return list;
//    }

    /**
     * Сортировка работников по полю <b>salary</b>
     * (Реализовано через Stream API)
     * @return список ({@link List}) отсортированных работников
     */
    public List<Employee> sortBySalary(){
        return employees
                .stream()
                .sorted(new EmplloyeeSalaryComparator())
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
