package employee;

import java.util.Comparator;


/**
 * Компаратор реализует сравнение работников
 * по полю <b>salary</b>
 *
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public class EmplloyeeSalaryComparator implements Comparator<Employee> {

    /**
     * реализация метода compare() интерфейса {@link Comparator}
     * @param e1 объект типа {@link Employee}
     * @param e2 объект типа {@link Employee}
     * @return 1, если e1.salary < e2.salary,
     *        -1, если e1.salary > e2.salary,
     *        0, если e1.salary = e2.salary,
     */
    @Override
    public int compare(Employee e1, Employee e2){
       if(e1.getSalary() > e2.getSalary()){
           return -1;
       } else if (e1.getSalary() < e2.getSalary()){
           return 1;
       } else {
           return 0;
       }
    }
}
