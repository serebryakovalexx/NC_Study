package employee;

/**
 * Перечисление описывает список возможных
 * должностей работников
 * @author Alex Serrebryakov
 * @version 1.0
 * @since 15.11.2017
 * @see Employee
 */
public enum Position {

    CLERK("clerk"),
    SALESMAN("salesman"),
    MANAGER("manager"),
    ANALYST("analyst"),
    PRESIDENT("president");

    private final String position;

//    Position(){}

    Position(String position){
        this.position = position;
    }

    public String stringName(){
        return position;
    }


    public static Position posByName(String position) throws IllegalArgumentException{
        switch (position.toLowerCase().trim()){
            case "clerk":
                return CLERK;
            case "salesman":
                return SALESMAN;
            case "manager":
                return MANAGER;
            case "analyst":
                return ANALYST;
            case "president":
                return PRESIDENT;
            default:
                throw new IllegalArgumentException();
        }
    }
}
