package employee;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;


public class EmployeeTest {

    private static List<Employee> employees = new ArrayList<>();

    @BeforeClass
    public static void beforeClass(){
        employees.add(new Employee("Alex", "Ivanov",
                "clerk", 50));
        employees.add(new Employee("Sasha", "Alekseev",
                "manager", 90));
        employees.add(new Employee("Nastya", "Kulikova",
                "president", 150));
        employees.add(new Employee("Alexey", "Grigoriev",
                "salesman", 80));
        employees.add(new Employee("Ksenya", "Nikolaeva",
                "analyst", 80));
        employees.add(new Employee("Marina", "Nikolaeva",
                "clerk", 75));
        employees.add(new Employee("Dmitri", "Morozov",
                "clerk", 46));
    }


    @Test
    public void testSearch(){
        List<Employee> list = new ArrayList<>();
        list.add(new Employee("Alex", "Ivanov",
                "clerk", 50));
        list.add(new Employee("Marina", "Nikolaeva",
                "clerk", 75));
        list.add(new Employee("Dmitri", "Morozov",
                "clerk", 46));
        EmployeeContainer container = new EmployeeContainer();
        container.setEmployees(employees);
        assertArrayEquals(list.toArray(), container.search("clerk").toArray());
    }

    @Test
    public void testSortByLastNameAndSalary(){
        List<Employee> list = new ArrayList<>();
        list.add(new Employee("Sasha", "Alekseev",
                "manager", 90));
        list.add(new Employee("Alexey", "Grigoriev",
                "salesman", 80));
        list.add(new Employee("Alex", "Ivanov",
                "clerk", 50));
        list.add(new Employee("Nastya", "Kulikova",
                "president", 150));
        list.add(new Employee("Dmitri", "Morozov",
                "clerk", 46));
        list.add(new Employee("Ksenya", "Nikolaeva",
                "analyst", 80));
        list.add(new Employee("Marina", "Nikolaeva",
                "clerk", 75));

        EmployeeContainer container = new EmployeeContainer();
        container.setEmployees(employees);
        assertArrayEquals(list.toArray(), container.sortByLastNameAndSalary().toArray());
    }

    @Test
    public void testSortByLastName(){
        List<Employee> list = new ArrayList<>();
        list.add(new Employee("Sasha", "Alekseev",
                "manager", 90));
        list.add(new Employee("Alexey", "Grigoriev",
                "salesman", 80));
        list.add(new Employee("Alex", "Ivanov",
                "clerk", 50));
        list.add(new Employee("Nastya", "Kulikova",
                "president", 150));
        list.add(new Employee("Dmitri", "Morozov",
                "clerk", 46));
        list.add(new Employee("Ksenya", "Nikolaeva",
                "analyst", 80));
        list.add(new Employee("Marina", "Nikolaeva",
                "clerk", 75));
        EmployeeContainer container = new EmployeeContainer();
        container.setEmployees(employees);
        assertArrayEquals(list.toArray(), container.sortByLastName().toArray());
    }

    @Test
    public void testSortBySalary(){
        List<Employee> list = new ArrayList<>();
        list.add(new Employee("Nastya", "Kulikova",
                "president", 150));
        list.add(new Employee("Sasha", "Alekseev",
                "manager", 90));
        list.add(new Employee("Alexey", "Grigoriev",
                "salesman", 80));
        list.add(new Employee("Ksenya", "Nikolaeva",
                "analyst", 80));
        list.add(new Employee("Marina", "Nikolaeva",
                "clerk", 75));
        list.add(new Employee("Alex", "Ivanov",
                "clerk", 50));
        list.add(new Employee("Dmitri", "Morozov",
                "clerk", 46));
        EmployeeContainer container = new EmployeeContainer();
        container.setEmployees(employees);
        assertArrayEquals(list.toArray(), container.sortBySalary().toArray());
    }

    @AfterClass
    public static void afterClass(){
        employees = null;
    }

}
