package employee;

import org.junit.Test;
import static org.junit.Assert.*;

public class PositionTest {

    @Test
    public void test0(){
        Position position = Position.posByName("clerk");
        assertEquals("clerk", position.stringName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1(){
        Position.posByName("aaaa");
    }

    @Test
    public void test2(){
        Position position = Position.valueOf("CLERK");
        assertEquals(position, Position.CLERK);
    }

    @Test
    public void test3(){
        assertArrayEquals(new Object[]{Position.CLERK, Position.SALESMAN,
                Position.MANAGER, Position.ANALYST,
        Position.PRESIDENT}, Position.values());
    }
}

