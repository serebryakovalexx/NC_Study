package figure;

import org.junit.Test;
import static org.junit.Assert.*;

public class PointTest {

    @Test
    public void test0(){
        Point point = new Point();
        point.setX(2);
        point.setY(5);
        assertArrayEquals(new double[]{2.0, 5.0},
                new double[]{point.getX(), point.getY()},
                0.05);
    }
}
