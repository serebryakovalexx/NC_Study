package figure;

import org.junit.Test;
import static org.junit.Assert.*;

public class RectangleTest {

    @Test(expected = IllegalArgumentException.class)
    public void test0(){
        new Rectangle(-5, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test01(){
        new Rectangle(5, -5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1(){ new Rectangle(7, -8, 5, -5); }

    @Test
    public void test1_1(){
        Rectangle rectangle = new Rectangle(7, 8, 5, -5);
        assertArrayEquals(new double[]{7, 8, 5, -5},
                new double[]{rectangle.getLength(), rectangle.getWidth(),
                rectangle.getCoordinates().getX(),
                rectangle.getCoordinates().getY()}, 0.05);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test2(){
        new Rectangle(5,5).scale(-5);
    }

    @Test
    public void test3(){
        Rectangle rectangle = new Rectangle(5,5);
        rectangle.scale(5);
        assertTrue(rectangle.getWidth() == 25
        && rectangle.getLength() == 25);
    }

    @Test
    public void test4(){
        assertEquals(30,  new Rectangle(5,6).area(), 0.05 );
    }


}
