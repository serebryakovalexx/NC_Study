package figure;

import org.junit.Test;
import static org.junit.Assert.*;

public class CircleTest {


    @Test(expected = IllegalArgumentException.class)
    public void test0(){
        new Circle(-5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1(){
        new Circle(-5, -5, -5);
    }

    @Test
    public void test2(){
        Circle circle = new Circle(5, -5, -5);
        assertTrue(circle.getRadius() == 5 &&
        circle.coordinates.getX() == -5 &&
        circle.coordinates.getY() == -5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test3(){
        Circle circle = new Circle(5);
        circle.scale(-5);
    }

    @Test
    public void test4(){
        Circle circle = new Circle(5);
        circle.scale(5);
        assertEquals(25, circle.getRadius(), 0.05);
    }

    @Test
    public void test5(){
        Circle circle = new Circle(5);
        assertEquals(Math.PI*circle.getRadius()*circle.getRadius(),
                circle.area(), 0.05);
    }

    @Test
    public void test6(){
        Circle circle0 = new Circle(5, 2, 3);
        Circle circle1 = new Circle(2, 1, 2);
        assertTrue(circle0.ckeck(circle1));
    }

    @Test
    public void test7(){
        Circle circle0 = new Circle(5, 20, -30);
        Circle circle1 = new Circle(2, 1, 2);
        assertFalse(circle0.ckeck(circle1));
    }

    @Test
    public void test8(){
        Circle circle = new Circle(5);
        Point point = new Point(1,2);
        assertTrue(circle.checkPoint(point));
    }

    @Test
    public void test9(){
        Circle circle = new Circle(5, 10, 20);
        Point point = new Point(1,2);
        assertFalse(circle.checkPoint(point));
    }

    @Test
    public void test10(){
        Circle circle = new Circle();
        circle.setRadius(5.0);
        assertEquals(5.0, circle.getRadius(), 0.05);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test11(){
        Circle circle = new Circle();
        circle.setRadius(-5.0);
    }

}
