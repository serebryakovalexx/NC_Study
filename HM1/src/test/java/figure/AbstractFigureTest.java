package figure;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AbstractFigureTest {

    /**
     * после каждого теста содержание
     * полей заглушки очищается автоматически
     */
    @Spy
    AbstractFigure figure;

    @Test
    public void test0(){
        figure.setCoordinates(new Point(4, 5));
        assertArrayEquals(new double[]{4,5},
                new double[]{figure.getCoordinates().getX(), figure.getCoordinates().getY()},
                0.05);
    }

    @Test
    public void test1(){
        figure.setCoordinates(new Point(4, 5));
        assertArrayEquals(new double[]{4, 5},
                new double[]{figure.show().getX(), figure.show().getY()}, 0.05);
    }

    @Test
    public void test2(){
        figure.setCoordinates(new Point(4, 5));
        figure.move(2,3);
        assertArrayEquals(new double[]{6, 8},
                new double[]{figure.show().getX(), figure.show().getY()}, 0.05);
    }
}
