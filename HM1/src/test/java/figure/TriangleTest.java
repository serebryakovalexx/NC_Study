package figure;

import org.junit.Test;
import static org.junit.Assert.*;

public class TriangleTest {

    @Test(expected = IllegalArgumentException.class)
    public void test0(){
        new Triangle(5, 1, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1(){
        new Triangle(3, -2, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test2(){
        new Triangle(5,4,2).scale(-5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test2_1(){
        new Triangle(5,4,22, -2, 5);
    }

    @Test
    public void test3(){
       Triangle triangle = new Triangle(5,4,2, -2, 5);
       triangle.scale(5);
       assertEquals(25, triangle.getFirstSide(), 0.05);
       assertEquals(20, triangle.getSecondSide(), 0.05);
       assertEquals(10, triangle.getThirdSide(), 0.05);
    }



    @Test
    public void test4(){
        Triangle triangle = new Triangle(5, 4, 2);
        assertEquals(3.79, triangle.area(), 0.05);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test5(){
        Triangle triangle = new Triangle(5,4,2);
        triangle.setFirstSide(-5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test6(){
        Triangle triangle = new Triangle(5,4,2);
        triangle.setSecondSide(-5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test7(){
        Triangle triangle = new Triangle(5,4,2);
        triangle.setThirdSide(-5);
    }

    @Test
    public void test8(){
        Triangle triangle = new Triangle(5,4,2);
        triangle.setFirstSide(2.5);
        assertEquals(2.5, triangle.getFirstSide(), 0.05);
    }

    @Test
    public void test9(){
        Triangle triangle = new Triangle(5,4,2);
        triangle.setSecondSide(3.5);
        assertEquals(3.5, triangle.getSecondSide(), 0.05);
    }

    @Test
    public void test10(){
        Triangle triangle = new Triangle(5,4,2);
        triangle.setThirdSide(3.5);
        assertEquals(3.5, triangle.getThirdSide(), 0.05);
    }
}
