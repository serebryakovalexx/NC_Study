<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/3.3/examples/signin/signin.css" rel="stylesheet">
    <title>Autherization</title>
</head>
<script language="JavaScript" type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.jshttps://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script language="JavaScript" type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<script language="JavaScript" type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#submit").click(function (event) {
            var username = $("#username").val();
            var password = $("#password").val();
            $.post("checkLogin", {username: username, password: password}, function (isChecked) {
                if (isChecked.toString() == "false") {
                    $("#username").removeClass("is-valid");
                    $("#password").removeClass("is-valid");
                    $("#errorText").removeClass("text-success");
                    $("#username").addClass("is-invalid");
                    $("#password").addClass("is-invalid");
                    $("#errorText").addClass("text-danger");
                    $("#errorText").text("Неверный логин или пароль");
                } else if (isChecked.toString() == "true"){
                    $("#username").removeClass("is-invalid");
                    $("#password").removeClass("is-invalid");
                    $("#errorText").removeClass("text-danger");
                    $("#username").addClass("is-valid");
                    $("#password").addClass("is-valid");
                    $("#errorText").addClass("text-success");
                    $("#errorText").text("Логин и пароль верны");
                    $(location).attr('href', '/orders')
                }
            });
        });
    });
</script>
<body>

<div class="wrapper">
    <form class="form-signin" method="post">

        <h2 class="form-signin-heading">Введите логин и пароль</h2>

            <div class="form-group">
                <input type="text" class="form-control" name="username" placeholder="Имя пользователя" id="username"
                       required="" autofocus=""/>
            </div>


            <div class="form-group" >
                <input type="password" class="form-control" name="password" placeholder="Пароль" id="password"
                       required=""/>
            </div>

        <div class="form-group">
            <input type="button" class="btn btn-lg btn-primary btn-block"  id="submit" value="Войти"/>
        </div>

        <div class="form-group">
            <div id="errorText" class="text-danger text-success"></div>
        </div>
    </form>
</div>

</body>
</html>
