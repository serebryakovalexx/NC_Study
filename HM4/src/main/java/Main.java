import com.google.gson.Gson;
import entities.Customer;
import entities.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import webutils.RegisterException;
import webutils.User;
import webutils.UserUtils;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class Main {
    public static void main(String[] args) throws RegisterException, InvalidKeySpecException, NoSuchAlgorithmException {
//        регистрируем админа
//        User user = new User("admin", "admin");
//        UserUtils userUtils = new UserUtils();
//        userUtils.register(user);
//        user = userUtils.find(user);
//        System.out.println(user.getUsername() + "\n" + user.getPassword());
//        регистрируем клиентов и их заказы

//        SessionFactory factory = new Configuration()
//                .configure()
//                .addPackage("entities")
//                .addAnnotatedClass(Customer.class)
//                .addAnnotatedClass(Order.class)
//                .buildSessionFactory();
//        Session session = factory.openSession();
//        Transaction transaction = session.beginTransaction();
//        Customer customer1 = new Customer("Alex", "45645456465");
//        Customer customer2 = new Customer("Denis", "8888888");
//        Order order1 = new Order(851316, 225.5);
//        Order order2 = new Order(99999, 555.4, customer1);
//        Order order3 = new Order(8888, 6666.5, customer1);
//        Order order4 = new Order(7777, 777.5, customer2);
//
//        session.save(customer1);
//        session.save(customer2);
//        session.save(order1);
//        session.save(order2);
//        session.save(order3);
//        session.save(order4);
//        Order order = session.get(Order.class, 7);
//        System.out.println(order.getCustomer().getFullName());
//
//        transaction.commit();
//        session.close();
//        factory.close();


        Customer customer = new Customer("qwerty", "123456");
        Order order = new Order(465456, 465.5, null);
        String json = new Gson().toJson(order);
        System.out.println(json);


    }
}
