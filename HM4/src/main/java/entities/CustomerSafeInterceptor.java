package entities;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.time.LocalDate;


public class CustomerSafeInterceptor extends EmptyInterceptor{


    @Override
    public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws RuntimeException{
        if (entity instanceof Customer){
            throw new RuntimeException("Customer in immutable entity");
        }
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) throws RuntimeException{
        if (entity instanceof Customer){
            throw new RuntimeException("Customer in immutable entity");
        }
        return false;
    }

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws RuntimeException {
        if (entity instanceof Customer){
            throw new RuntimeException("Customer in immutable entity");
        }
        return false;
    }

}
