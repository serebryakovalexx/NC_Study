package utils;

import entities.Customer;
import entities.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.postgresql.util.PSQLException;

import javax.persistence.NoResultException;
import java.util.List;

public class DBActions {

    public boolean addOrder(Long orderNumber, Double price, String customerName){
        boolean isSucceed = false;
        SessionFactory factory = SFactory.getFactory();
        try(Session session = factory.openSession()){
            Transaction transaction = session.beginTransaction();
            Order order;
            try {
                if(!customerName.trim().equals("null")) {
                    String hql = "FROM Customer c WHERE c.fullName=:username";
                    Query query = session.createQuery(hql);
                    query.setCacheable(true);
                    query.setParameter("username", customerName);
                    Customer customer = (Customer) query.getSingleResult();
                    order = new Order(orderNumber, price, customer);
                    session.save(order);
                    isSucceed = true;
                    transaction.commit();
                    session.close();
                } else {
                    order = new Order(orderNumber, price, null);
                    session.save(order);
                    isSucceed = true;
                    transaction.commit();
                    session.close();
                }
            } catch (Exception e){}
        }
        return isSucceed;
    }

    public boolean delete(Long orderNumber){

        boolean isSucceed = false;

        SessionFactory factory = SFactory.getFactory();
        try(Session session = factory.openSession()){
            Transaction transaction = session.beginTransaction();
            try {
                String hql = "FROM Order o WHERE o.number=:orderNumber";
                Query query = session.createQuery(hql);
                query.setCacheable(true);
                query.setParameter("orderNumber", orderNumber);
                Order order = (Order) query.getSingleResult();
                session.delete(order);
                isSucceed = true;
                transaction.commit();
                session.close();
            } catch (Exception e){}
        }
        return isSucceed;
    }

    public List orderList(){
        List orders = null;
        SessionFactory factory = SFactory.getFactory();
        try (Session session = factory.openSession()){
            String hql = "FROM Order";
            Query query = session.createQuery(hql);
            query.setCacheable(true);
            Transaction transaction = session.beginTransaction();
            orders = query.getResultList();
            transaction.commit();
            session.close();
        }
        return orders;
    }

    public List customerList(){
        List customers = null;
        SessionFactory factory = SFactory.getFactory();
        try(Session session = factory.openSession()){
            String hql = "FROM Customer";
            Query query = session.createQuery(hql);
            query.setCacheable(true);
            Transaction transaction = session.beginTransaction();
            customers = query.getResultList();
            transaction.commit();
            session.close();
        }
        return customers;
    }
}
