package utils;

import entities.Customer;
import entities.CustomerSafeInterceptor;
import entities.Order;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SFactory {

    private static volatile SessionFactory factory;

    private SFactory(){}

    public static SessionFactory getFactory(){
        SessionFactory sessionFactory = factory;
        if(sessionFactory == null){
            synchronized (SFactory.class){
                sessionFactory = factory;
                if (sessionFactory == null){
                    factory = sessionFactory =
                            new Configuration()
                            .configure()
                            .addPackage("entities")
                            .addAnnotatedClass(Customer.class)
                            .addAnnotatedClass(Order.class)
                            .setInterceptor(new CustomerSafeInterceptor())
                            .buildSessionFactory();
                }
            }
        }
        return sessionFactory;
    }

}
