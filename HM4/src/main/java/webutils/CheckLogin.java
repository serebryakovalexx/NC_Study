package webutils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@WebServlet(urlPatterns = "/checkLogin", name = "checkLogin")
public class CheckLogin extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String username = req.getParameter("username").trim();
        String password = req.getParameter("password").trim();

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        User formUser = new User(username, password);
        User userFromDB = null;

        UserUtils userUtils = new UserUtils();
        try {
            userFromDB = userUtils.find(formUser);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {}

        if (userFromDB != null) {
            req.getSession().setAttribute("user", userFromDB);
            resp.getWriter().write("true");
        } else {
            resp.getWriter().write("false");
        }
    }
}
