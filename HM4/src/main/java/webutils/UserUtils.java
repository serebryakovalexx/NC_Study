package webutils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class UserUtils {

    public void register(User regUser) throws RegisterException {

        User user = null;

        try {
            user = new User(regUser.getUsername(), encodePassword(regUser.getPassword()));
        } catch (PasswordException e) {
            e.printStackTrace();
        }

        if (user == null) throw new RegisterException("Error during register");

        try (SessionFactory factory = new Configuration()
                .configure()
                .addPackage("webutils")
                .addAnnotatedClass(User.class)
                .buildSessionFactory()) {
            try (Session session = factory.openSession()) {
                Transaction transaction = session.beginTransaction();
                session.save(user);
                transaction.commit();
            }
        }
    }

    public User find(User formUser) throws InvalidKeySpecException, NoSuchAlgorithmException {

        try (SessionFactory factory = new Configuration()
                .configure()
                .addPackage("webutils")
                .addAnnotatedClass(User.class)
                .buildSessionFactory()) {

            try (Session session = factory.openSession()) {
                String hql = "FROM User u WHERE u.username=:username";
                Query query = session.createQuery(hql);
                query.setParameter("username", formUser.getUsername());
                User userFromDB = null;
                try {
                    userFromDB = (User) query.getSingleResult();
                } catch (NoResultException e){
                    return null;
                }

                if (validatePassword(formUser.getPassword(), userFromDB.getPassword())) {
                    return userFromDB;
                } else {
                    return null;
                }
            }
        }
    }

    private String encodePassword(String password) throws PasswordException {
        String encodePass = null;
        try {
            encodePass = generateStrongPasswordHash(password);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        if (encodePass == null) throw new PasswordException("Error during generating password");
        return encodePass;
    }

    private String generateStrongPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    private byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    private boolean validatePassword(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    private byte[] fromHex(String hex) throws NoSuchAlgorithmException {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
