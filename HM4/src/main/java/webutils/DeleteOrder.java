package webutils;

import com.google.gson.Gson;
import entities.Order;
import utils.DBActions;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/deleteOrder", name = "deleteOrder")
public class DeleteOrder extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long orderNumber = Long.valueOf(req.getParameter("orderNumber").trim());

        resp.setContentType("application/json");
        resp.setContentType("UTF-8");

        DBActions actions = new DBActions();
        List<Order> orders = null;

        boolean isSucceed;

        isSucceed = actions.delete(orderNumber);

        if (isSucceed){
            orders = (List<Order>) actions.orderList();
        }

        String json = new Gson().toJson(orders);
        resp.getWriter().write(json);
    }
}
