package webutils;

import com.google.gson.Gson;
import entities.Order;
import utils.DBActions;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/addOrder", name = "addOrder")
public class AddOrder extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long orderNumber = null;
        Double price = null;

        try {
            orderNumber = Long.valueOf(req.getParameter("orderNumber").trim());
            price = Double.valueOf(req.getParameter("price").trim());
        } catch (NumberFormatException e) {}

        String customerName = req.getParameter("customerName").trim();

        resp.setContentType("application/json");
        resp.setContentType("UTF-8");
        DBActions actions = new DBActions();
        List<Order> orders = null;

        boolean isSucceed;

        if(orderNumber != null && price != null){
            isSucceed = actions.addOrder(orderNumber, price, customerName);
            if(isSucceed){
                orders = (List<Order>) actions.orderList();
            }
        }

        String json = new Gson().toJson(orders);
        resp.getWriter().write(json);
    }
}
