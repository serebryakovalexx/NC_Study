<%@ page import="java.util.Iterator" %>
<%@ page import="utils.DBActions" %>
<%@ page import="entities.Order" %>
<%@ page import="webutils.User" %>
<%@ page import="entities.Customer" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Orders</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"
        integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D"
        crossorigin="anonymous"></script>
<script language="JavaScript" type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#addOrder").click(function (event) {
            var orderNumber = $("#orderNumber").val();
            var price = $("#price").val();
            var customerName = $("#customerName option:selected").val();
            $.post("addOrder", {
                orderNumber: orderNumber,
                price: price,
                customerName: customerName
            }, function (jsonResult) {
                if (jsonResult == "null") {
                    $("#textMessage").removeClass("text-success");
                    $("#textMessage").addClass("text-danger");
                    $("#textMessage").text("Не удалось добавить заказ");
                    $("#orders").css("display", "");
                    $("#ajaxOrders").css("display", "none");
                    $("#orderNumber").val('');
                    $("#price").val('');
                    $("#customerName").val("null");
                } else {
                    $("#textMessage").removeClass("text-danger");
                    $("#textMessage").addClass("text-success");
                    $("#textMessage").text("Заказ добавлен");
                    $("#ajaxOrders > tbody > tr").remove();
                    $.each(JSON.parse(jsonResult), function (key, value) {
                        $("<tr>").appendTo($("#ajaxOrders > tbody"))
                            .append($("<td>").text(value.number))
                            .append($("<td>").text(value.date.year + "-" + value.date.month + "-" + value.date.day))
                            .append($("<td>").text(value.price))
                            .append($("<td>").text(typeof value.customer == 'undefined' ? "" : value.customer.fullName))
                            .append($("<td>").text(typeof value.customer == 'undefined' ? "" : value.customer.phoneNumber))
                            .append($("<td>").append($('<input type="button" class="btn btn-sm btn-danger deleteOrder" value="Удалить" />').attr('data-number', value.number)));
                    });
                    $("#orders").css("display", "none");
                    $("#ajaxOrders").css("display", "");
                    $("#orderNumber").val('');
                    $("#price").val('');
                    $("#customerName").val("null");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function (event) {
        $(document).on('click', '.deleteOrder', function (event) {
            var orderNumber = $(this).data('number');
            console.log(orderNumber);
            $.post("deleteOrder", {orderNumber:orderNumber}, function (jsonResult) {
                if(jsonResult == "null"){
                    $("#textMessage").removeClass("text-success");
                    $("#textMessage").addClass("text-danger");
                    $("#textMessage").text("Не удалось удалить заказ");
                    $("#orders").css("display", "");
                    $("#ajaxOrders").css("display", "none");
                } else {
                    $("#textMessage").removeClass("text-danger");
                    $("#textMessage").addClass("text-success");
                    $("#textMessage").text("Заказ удалён");
                    $("#ajaxOrders > tbody > tr").remove();
                    $.each(JSON.parse(jsonResult), function (key, value) {
                        $("<tr>").appendTo($("#ajaxOrders > tbody"))
                            .append($("<td>").text(value.number))
                            .append($("<td>").text(value.date.year + "-" + value.date.month + "-" + value.date.day))
                            .append($("<td>").text(value.price))
                            .append($("<td>").text(typeof value.customer == 'undefined' ? "" : value.customer.fullName))
                            .append($("<td>").text(typeof value.customer == 'undefined' ? "" : value.customer.phoneNumber))
                            .append($("<td>").append($('<input type="button" class="btn btn-sm btn-danger deleteOrder" value="Удалить" />').attr('data-number', value.number)));
                    });
                    $("#orders").css("display", "none");
                    $("#ajaxOrders").css("display", "");
                }
            });
        });
    });
</script>

<body>
<%! DBActions actions = new DBActions(); %>
<h3 style="margin: 2%">Список заказов</h3></br>
<h4 style="margin-left: 2%">Добавить заказ:</h4>

<div class="wrapper">
    <form class="form-signin " method="post">
        <div class="row" style="margin-left: 2%; margin-top: 2%">
            <div class="form-group col">
                <input type="text" class="form-control" name="orderNumber" placeholder="Номер заказа" id="orderNumber"
                       required="" autofocus=""/>
            </div>
            <div class="form-group col">
                <input type="text" class="form-control" name="price" placeholder="Цена заказа" id="price"
                       required=""/>
            </div>
            <h5 style="padding-top: 0.2%">Клиент:</h5>
            <div class="form-group col-md-2" style="margin-left: 1%">
                <select class="custom-select" id="customerName">
                    <option value="null">null</option>
                    <%
                        for (Iterator iterator = actions.customerList().iterator(); iterator.hasNext(); ) {
                            Customer customer = (Customer) iterator.next();
                    %>
                    <option><%= customer.getFullName()%>
                    </option>
                    <%}%>
                </select>
            </div>
            <div class="form-group col">
                <input type="button" class="btn btn-sm btn-success" id="addOrder" value="Добавить"/>
            </div>
        </div>
        <div class="form-group col">
            <div id="textMessage" class="text-danger text-success"></div>
        </div>
    </form>
</div>

<div style="margin: 2%">
    <table class="table table-hover table-bordered" id="orders">
        <thead>
        <tr>
            <th>Номер заказа</th>
            <th>Дата заказа</th>
            <th>Цена заказа</th>
            <th>Имя клиента</th>
            <th>Номер телефона</th>
            <th>Удалить заказ</th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Iterator iterator = actions.orderList().iterator(); iterator.hasNext(); ) {
                Order order = (Order) iterator.next();
        %>
        <tr>
            <td><%= order.getNumber()%>
            </td>
            <td><%= order.getDate()%>
            </td>
            <td><%= order.getPrice()%>
            </td>
            <c:choose>
                <c:when test="<%= order.getCustomer() != null %>">
                    <td><%= order.getCustomer().getFullName()%>
                    </td>
                    <td><%= order.getCustomer().getPhoneNumber()%>
                    </td>
                </c:when>
                <c:when test="<%=  order.getCustomer() == null%>">
                    <td></td>
                    <td></td>
                </c:when>
            </c:choose>
            <td><input type="button" class="btn btn-sm btn-danger deleteOrder" value="Удалить"
                       data-number="<%= order.getNumber()%>"/></td>
        </tr>
        <%}%>
        </tbody>
    </table>

    <table class="table table-hover table-bordered" style="display: none" id="ajaxOrders">
        <thead>
        <tr>
            <th>Номер заказа</th>
            <th>Дата заказа</th>
            <th>Цена заказа</th>
            <th>Имя клиента</th>
            <th>Номер телефона</th>
            <th>Удалить заказ</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

</body>
</html>
