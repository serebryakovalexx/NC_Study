<%--
список источников:
http://www.programming-free.com/2012/08/ajax-with-jsp-and-servlet-using-jquery.html
https://stackoverflow.com/questions/4112686/how-to-use-servlets-and-ajax
https://www.journaldev.com/4742/jquery-ajax-jsp-servlet-java-example
https://www.youtube.com/watch?v=zY2F_pCSJQ8
https://www.youtube.com/watch?v=P4eOHI6OGks
 --%>

<%@page contentType="text/html" %>
<html>
<head>
    <title>AJAX with JSP</title>
</head>
<%-- либо можно указать адрес страницы с исходным кодом --%>
<script src="jquery-3.2.1.js"></script>
<script>
    $(document).ready(function () {
        $('#submit').click(function(event) {
            var username=$('#user').val();
//          лучше отправлять через метод POST, т.к. через GET видны перредаваемые параметры
            $.post('ajaxHandler', {user:username}, function(responseText) {
                $('#welcometext').text(responseText);
            });
        });
    });
</script>
<body>
<h2>AJAX with JSP</h2>
<form id="form1">
    Enter your name:
    <input type="text" id="user"/>
    <input type="button" id="submit" value="Ajax Submit"/>
    <div id="welcometext"></div>
</form>


</body>
</html>