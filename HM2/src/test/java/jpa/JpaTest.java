package jpa;

import entities.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class JpaTest {

    private static SessionFactory factory;

    @Before
    public void before(){

        factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        String deleteQuery = "DELETE FROM Employee";
        session.createQuery(deleteQuery).executeUpdate();
        transaction.commit();

        if(transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();

    }

    @Test
    public void testCreate(){

        Employee employee = new Employee("Alex", 50);

        new HibernateCrudImpl().create(employee.getName(), employee.getSalary());

        factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        String selectQuery = "FROM Employee";
        List employees = session.createQuery(selectQuery).list();

        String actualName = null;
        double actualSalary = -1;
        for(Iterator iterator = employees.iterator(); iterator.hasNext();){
            Object obj = iterator.next();
            if(obj instanceof Employee){
               actualName = ((Employee) obj).getName();
                actualSalary = ((Employee) obj).getSalary();
            }
        }

        if(transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();

        assertTrue(employee.getName().equals(actualName.trim()) &&
        employee.getSalary() == actualSalary);
    }

    @Test
    public void testRead(){

        List<Employee> expectedEmployees = new LinkedList<>();
        expectedEmployees.add(new Employee("Alex", 50));
        expectedEmployees.add(new Employee("Sasha", 30));
        expectedEmployees.add(new Employee("Ksenia", 80));

        List<Employee> actualEmployees = null;

        factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        expectedEmployees.forEach(session::save);
        transaction.commit();

        actualEmployees = new LinkedList<>(new HibernateCrudImpl().read());

        if(transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();

        assertArrayEquals(expectedEmployees.toArray(), actualEmployees.toArray());

    }

//  TODO: странное поведение теста
    @Test
    public void testUpdate(){

        Employee employee = new Employee("Alex", 50);
        double expectedSalary = 20;
        double actualSalary = -1;

        factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(employee);
        transaction.commit();
        /**
         * нужно закрывать сессию при каждом изменении в базе,
         * чтобы данные подтягивались не с
         * кэша(в данном случае первого уровня), а с базы
         * */
        session.close();

        new HibernateCrudImpl().update(employee.getName(), employee.getSalary(), expectedSalary);

        session = factory.openSession();
        transaction = session.beginTransaction();
        String hqlSelect = "FROM Employee";
        List employees = session.createQuery(hqlSelect).list();
        for (Iterator iterator = employees.iterator(); iterator.hasNext();){
            Object obj = iterator.next();
            if (obj instanceof Employee){
                actualSalary = ((Employee) obj).getSalary();

            }
        }
        transaction.commit();


        if (transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();

        assertEquals(expectedSalary, actualSalary, 0.05);
    }

    @Test
    public void testDelete(){
        Employee employee = new Employee("Alex", 50);
        int beforeDelete = 0;
        int afterDelete = 0;

        factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(employee);
        transaction.commit();
        session.close();

        session = factory.openSession();
        transaction = session.beginTransaction();
        String hqlSelect = "FROM Employee";
        List employees = session.createQuery(hqlSelect).list();
        for (Iterator iterator = employees.iterator(); iterator.hasNext();){
            Object obj = iterator.next();
            if (obj instanceof Employee){
                beforeDelete++;
            }
        }
        transaction.commit();
        session.close();

        new HibernateCrudImpl().delete(employee.getName(), employee.getSalary());

        session = factory.openSession();
        transaction = session.beginTransaction();
        List employees1 = session.createQuery(hqlSelect).list();
        for (Iterator iterator = employees1.iterator(); iterator.hasNext();){
            Object obj = iterator.next();
            if (obj instanceof Employee){
                afterDelete++;
            }
        }
        transaction.commit();
        session.close();
        factory.close();

        assertTrue(beforeDelete == 1 && afterDelete == 0);
    }

    @AfterClass
    public static void afterClass(){

        factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        String deleteQuery = "DELETE FROM Employee";
        session.createQuery(deleteQuery).executeUpdate();
        transaction.commit();

        if(transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();

    }

}
