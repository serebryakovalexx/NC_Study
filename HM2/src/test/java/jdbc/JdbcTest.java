package jdbc;

import entities.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static jdbc.DBConstants.*;
import static org.junit.Assert.*;

public class JdbcTest {

    private static Connection connection;
    private static Logger logger = LogManager.getLogger(JdbcTest.class);

    @Before
    public void before(){
        try {
            connection = DriverManager.getConnection(PSQL_URL, PSQL_USER, PSQL_PASSOWORD);
            logger.info("Starting DELETE query");
            String deleteQuery = "DELETE FROM " + TABLE_NAME;
            Statement stmt = connection.createStatement();

            int result = stmt.executeUpdate(deleteQuery);
            if (result > 0){
                logger.info(result + " row(s) deleted");
            } else if (result == 0){
                logger.warn(result + " row(s) deleted");
            } else {
                logger.error("Result is negative: result=" + result);
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreate(){

        Employee employee = new Employee("Alex", 50);

        try {
            new JdbcCrudImpl().create(employee.getName(), employee.getSalary());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Statement stmt = null;
        String name = null;
        double salary = -1;
        try {

            stmt = connection.createStatement();
            String selectQuery = "SELECT * FROM " + TABLE_NAME;
            ResultSet rs = stmt.executeQuery(selectQuery);

            while (rs.next()){
                name = rs.getString("name");
                salary = rs.getDouble("salary");
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();

        }
        assertTrue(employee.getName().equals(name.trim()) &&
        employee.getSalary() == salary);
    }

    @Test
    public void testRead(){
        List<Employee> expectedEmployees = new LinkedList<>();
        expectedEmployees.add(new Employee("Alex", 50));
        expectedEmployees.add(new Employee("Sasha", 30));
        expectedEmployees.add(new Employee("Ksenia", 80));

        List<Employee> actualEmployees = null;

        try {

            connection.setAutoCommit(false);
            String insertQuery = "INSERT INTO " + TABLE_NAME +
                    " VALUES(?,?)";
            PreparedStatement stmt = connection.prepareStatement(insertQuery);

            expectedEmployees.forEach(employee -> {
                try {
                    stmt.setString(1, employee.getName());
                    stmt.setDouble(2, employee.getSalary());
                    stmt.addBatch();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });

            stmt.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);

            actualEmployees = new LinkedList<>(new JdbcCrudImpl().read());

        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertArrayEquals(expectedEmployees.toArray(), actualEmployees.toArray());
    }

    @Test
    public void testUpdate(){

        Employee employee = new Employee("Alex", 50);
        double expectedSalary = 20;
        double actualSalary = -1;

        try {

            String insertQuery = "INSERT INTO " + TABLE_NAME +
                    " VALUES(?,?)";
            PreparedStatement stmt = connection.prepareStatement(insertQuery);
            stmt.setString(1, employee.getName());
            stmt.setDouble(2, employee.getSalary());
            stmt.executeUpdate();
            stmt.close();

            new JdbcCrudImpl().update(employee.getName(), employee.getSalary(), expectedSalary);

            String selectQuery = "SELECT * FROM " + TABLE_NAME +
                    " WHERE name=" + "'" + employee.getName() + "'";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(selectQuery);

            while (rs.next()){
                actualSalary = rs.getDouble("salary");
            }

            rs.close();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(expectedSalary, actualSalary, 0.05);
    }

    @Test
    public void testDelete(){
        Employee employee = new Employee("Alex", 50);
        int beforeDelete = 0;
        int afterDelete = 0;

        try {

            String insertQuery = "INSERT INTO " +
                    TABLE_NAME + " VALUES(?,?)";
            PreparedStatement stmt = connection.prepareStatement(insertQuery);
            stmt.setString(1, employee.getName());
            stmt.setDouble(2, employee.getSalary());
            stmt.executeUpdate();
            stmt.close();

            String selectQuery = "SELECT * FROM " + TABLE_NAME;
            Statement statement1 = connection.createStatement();

            ResultSet rs1 = statement1.executeQuery(selectQuery);
            while(rs1.next()){
                beforeDelete++;
            }
            new JdbcCrudImpl().delete(employee.getName(), employee.getSalary());

            ResultSet rs2 = statement1.executeQuery(selectQuery);
            while (rs2.next()){
                afterDelete++;
            }

            rs1.close();
            rs2.close();
            statement1.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertTrue(beforeDelete == 1 && afterDelete == 0);
    }

    @After
    public void after(){
        try{
            connection.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void afterClass(){

        try {
            connection = DriverManager.getConnection(PSQL_URL, PSQL_USER, PSQL_PASSOWORD);
            logger.info("Starting DELETE query");

            String deleteQuery = "DELETE FROM " + TABLE_NAME;
            Statement stmt = connection.createStatement();
            int result = stmt.executeUpdate(deleteQuery);

            if (result > 0){
                logger.info(result + " row(s) deleted");
            } else if (result == 0){
                logger.warn(result + " row(s) deleted");
            } else {
                logger.error("Result is negative: result=" + result);
            }

            stmt.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
