package utils;

import entities.Employee;

import java.sql.SQLException;
import java.util.List;

/**
 * Интерфейс, описывающий методы CRUD(create, read, update, delete)
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public interface CRUD {

    /**
     * Создание записи сотрудника в БД
     * @param name имя сотрудника, тип {@link String}
     * @param salary зарплата сотрудника, тип {@code double}
     * @throws SQLException
     * @see Employee
     */
    void create(String name, double salary) throws SQLException;

    /**
     * Вывод информации о всех сотрудниках
     * @return список ({@link List}) всех сотрудников,
     *         если сотрудников нет, то возвращается пустой список
     * @throws SQLException
     * @see Employee
     */
    List<Employee> read() throws SQLException;

    /**
     * Обновление зарплаты сотрудника в БД, соответствующего определённому
     * имени и зарплате
     * @param name имя сотрудника, тип {@link String}
     * @param salary зарплата сотрудника, тип {@code double}
     * @param newSalary новая зарплата сотрудника, тип {@code double}
     * @throws SQLException
     * @see Employee
     */
    void update(String name, double salary, double newSalary) throws SQLException;

    /**
     * Удаление записи сотрудника в БД, соответствующего определённому
     * имени и зарплате
     * @param name имя сотрудника, тип {@link String}
     * @param salary зарплата сотрудника, тип {@code double}
     * @throws SQLException
     * @see Employee
     */
    void delete(String name, double salary) throws SQLException;

}
