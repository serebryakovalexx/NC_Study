package jdbc;

/**
 * Константы для подключения к БД
 *
 *      TABLE_NAME - имя таблицы
 *      PSQL_URL - адресс доступа к БД
 *      PSQL_USER - имя пользователя в БД
 *      PSQL_PASSWORD - пароль пользователя в БД
 *
 * @author Alex Serebryakov
 * @version 1.0
 * @since 15.11.2017
 */
public final class DBConstants {

    private DBConstants(){}

    public static final String TABLE_NAME = "employeejdbc";
    public static final String PSQL_URL = "jdbc:postgresql://localhost:5432/postgres";
    public static final String PSQL_USER = "postgres";
    public static final String PSQL_PASSOWORD = "user";

}
