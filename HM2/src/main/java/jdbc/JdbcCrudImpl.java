package jdbc;

import entities.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.CRUD;

import javax.sql.RowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static jdbc.DBConstants.*;

public class JdbcCrudImpl implements CRUD {

    private static Logger logger = LogManager.getLogger(JdbcCrudImpl.class);

    @Override
    public void create(String name, double salary) throws SQLException {

        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            logger.info("Starting INSERT query");
            connection = DriverManager.getConnection(PSQL_URL, PSQL_USER, PSQL_PASSOWORD);
            String insertQuery = "INSERT INTO " + TABLE_NAME + " VALUES(?,?);";

            stmt = connection.prepareStatement(insertQuery);
            stmt.setString(1, name);
            stmt.setDouble(2, salary);

            int result = stmt.executeUpdate();

            if (result > 0){
                logger.info("Insert " + result + " row(s)");
            } else if (result == 0){
                logger.warn("Insert " + result + " rows");
            } else {
                logger.error("Result is negative: result=" + result);
            }

            stmt.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null && !stmt.isClosed()) stmt.close();
            if (connection != null && !connection.isClosed()) connection.close();
        }
    }

    @Override
    public List<Employee> read() throws SQLException {

        RowSet rs = null;
        List<Employee> employees = null;

        try {
            logger.info("Starting SELECT query");
            rs = RowSetProvider.newFactory().createJdbcRowSet();
            rs.setUrl(PSQL_URL);
            rs.setUsername(PSQL_USER);
            rs.setPassword(PSQL_PASSOWORD);
            String selectQuery = "SELECT * FROM " + TABLE_NAME + ";";
            rs.setCommand(selectQuery);
            rs.execute();

            employees = new LinkedList<>();
            int result = 0;
            while(rs.next()){
                result++;
                employees.add(new Employee(
                        rs.getString("name").trim(),
                        rs.getDouble("salary")
                ));
            }

            if(result > 0){
                logger.info("SELECT returns " + result + "rows");
            } else {
                logger.warn("SELECT returns " + result + "rows");
            }

            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if  (rs != null) rs.close();
        }

        return employees;
    }

    @Override
    public void update(String name, double salary, double newSalary) throws SQLException {

        Connection connection = null;
        PreparedStatement stmt = null;

        try {

            logger.info("Starting UPDATE query");
            connection = DriverManager.getConnection(PSQL_URL, PSQL_USER, PSQL_PASSOWORD);
            String updateQuery = "UPDATE " + TABLE_NAME +
                    " SET salary=? " +
                    "WHERE name=? AND salary=?;";
            stmt = connection.prepareStatement(updateQuery);
            stmt.setDouble(1, newSalary);
            stmt.setString(2,  name );
            stmt.setDouble(3, salary);

            int result = stmt.executeUpdate();
            if (result > 0){
                logger.info("Update " + result + " row(s)");
            } else if (result == 0){
                logger.warn("Update " + result + " rows");
            } else {
                logger.error("Result is negative: result=" + result);
            }

            stmt.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null && !stmt.isClosed()) stmt.close();
            if (connection != null && !connection.isClosed()) connection.close();
        }
    }

    @Override
    public void delete(String name, double salary) throws SQLException {

        Connection connection = null;
        PreparedStatement stmt = null;

        try {

            logger.info("Starting DELETE query");
            connection = DriverManager.getConnection(PSQL_URL, PSQL_USER, PSQL_PASSOWORD);
            String deleteQuery = "DELETE FROM " + TABLE_NAME +
                    " WHERE name=? AND salary=?";
            stmt = connection.prepareStatement(deleteQuery);
            stmt.setString(1, name);
            stmt.setDouble(2, salary);

            int result = stmt.executeUpdate();

            if (result > 0){
                logger.info("Delete " + result + " row(s)");
            } else if (result == 0){
                logger.warn("Delete " + result + " rows");
            } else {
                logger.error("Result is negative: result=" + result);
            }

            stmt.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null && !stmt.isClosed()) stmt.close();
            if (connection != null && !connection.isClosed()) connection.close();
        }
    }
}
