package jpa;

import entities.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import utils.CRUD;

import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

public class HibernateCrudImpl implements CRUD {

    @Override
    public void create(String name, double salary) {

        SessionFactory factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(new Employee(name, salary));

        transaction.commit();

        if(transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();

    }

    @Override
    public List<Employee> read() {

        SessionFactory factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        String hqlSelectQuery = "FROM Employee";
        List employees = session.createQuery(hqlSelectQuery).list();

        List<Employee> result = new LinkedList<>();
        employees.forEach(employee -> {
            if (employee instanceof Employee){
                result.add((Employee) employee);
            }
        });

        transaction.commit();

        if(transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();

        return result;
    }

    @Override
    public void update(String name, double salary, double newSalary) {

        SessionFactory factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        String hqlSelect = "FROM Employee " +
                "WHERE name = :name AND salary = :salary";
        Query query = session.createQuery(hqlSelect);
        query.setParameter("name", name)
                .setParameter("salary", salary);
        Employee employee =  (Employee) query.getSingleResult();
        employee.setSalary(newSalary);
        session.update(employee);
        transaction.commit();

        if (transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();
    }

    @Override
    public void delete(String name, double salary) {

        SessionFactory factory = new Configuration()
                .configure()
                .addPackage("entities")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        String hqlDelete = "FROM Employee " +
                "WHERE name = :name AND salary = :salary";
        Query query = session.createQuery(hqlDelete);
        query.setParameter("name", name)
                .setParameter("salary", salary);
        Employee employee = (Employee)query.getSingleResult();
        session.delete(employee);
        transaction.commit();

        if (transaction.getStatus() == TransactionStatus.ACTIVE) transaction.rollback();
        session.close();
        factory.close();
    }
}
